#!/bin/bash

# run this first:
# stty -F /dev/ttyUSB0 cs8 9600 ignbrk -brkint -icrnl -imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe -echok -echoctl -echoke noflsh -ixon -crtscts

echo "000000" > /dev/ttyUSB0
echo "000000"

for (( i=1; i<=10; i++ ))
do
	echo "110000" > /dev/ttyUSB0
	echo "110000"
	sleep 1
	echo "000011" > /dev/ttyUSB0
	echo "000011"
	sleep 1
done

