#!/bin/bash

~/rekelbox/initPort.sh

# Wait 10 seconds to initialize
sleep 10

# Create screen session rekelbox (-S) without attaching (-dm)
screen -dmS rekelbox
sleep 1

# create a new window (window 1) from window 0
screen -S rekelbox -p 0 -X stuff "screen$(printf \\r)" # on a bash prompt instead of $(printf \\r) do ctrl+V enter, now ^M will show

# send "cat </dev/ttyUSB0" command to rekelbox, window 0
screen -S rekelbox -p 0 -X stuff "cat </dev/ttyUSB0$(printf \\r)"

# send "watch -n1 ./sendCpuPerc.sh" command to rekelbox, window 1
screen -S rekelbox -p 1 -X stuff "watch -n1 ~/rekelbox/sendCpuPerc.sh$(printf \\r)"

