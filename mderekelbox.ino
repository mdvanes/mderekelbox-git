/*
    MDERekelBox
    https://bitbucket.org/mdvanes/mderekelbox/

    @author M.D. van Es
    1-9-2015

    based on http://arduino.cc/en/Tutorial/Blink by David Cuartielles
    based on an orginal by H. Barragan for the Wiring i/o board

    plug one board into gnd to 8
    plug second board into 7 to 1
 
 */

// What pins are reserved for LEDs
// Matrix of ledPins => ledState
// LOW is a boolean and boolean < int, so this should work
const int nrOfLeds = 12;
boolean isMonitoring = true;
boolean isDemoMode = false;
long previousMillis = 0;
int incomingByte;
int leds[nrOfLeds][2] = {
    // Red
    {13, LOW}, // Pin 0 and 1 are used for communication along with USB when Serial is used, can't be used for LED
    {12, LOW},
    {11, LOW},
    {10, LOW},
    {9, LOW},
    {8, LOW},
    // Green
    {6, LOW},
    {5, LOW},
    {4, LOW},
    {3, LOW},
    {2, LOW},
    {1, LOW}
};
int ledsOLD[nrOfLeds][2] = {
    // Red
    {1, LOW}, // Pin 0 and 1 are used for communication along with USB when Serial is used, can't be used for LED
    {2, LOW},
    {3, LOW},
    {4, LOW},
    {5, LOW},
    {6, LOW},
    // Green
    {8, LOW},
    {9, LOW},
    {10, LOW},
    {11, LOW},
    {12, LOW},
    {13, LOW}
};
int GND = 7;//GND
int previousSensorVal = HIGH;

void setup() {

    // Initialize extra GND pin
    pinMode(GND, OUTPUT);  
    digitalWrite(GND, LOW); 
  
    Serial.begin(9600);
    Serial.println("Start setup");
    // Initialize the digital pins as output
    for( int i = 0; i < nrOfLeds; i++ ) {
        // Note to self: Processing can't assign an array to a variable. *led is a 
        // pointer alias to leds[i] and so changes in led will change leds[i] 
        // (which is fine in this case)
        int *led = leds[i];
        int ledPin = led[0];
        pinMode(ledPin, OUTPUT);
    }
    
    // Configure pin2 as an input and enable the internal pull-up resistor
    //pinMode(2, INPUT_PULLUP);
  
    Serial.println("Done initializing Rekelb0x");
    //incrementLeds(500);
    // Turn on one by one
    rotateLeds(80);
    // Hack to turn all on
    //incrementLeds(1);
}

void loop() {
    // Read the pushbutton value into a variable
    //int sensorVal = digitalRead(2);
  
    //if(sensorVal == HIGH) {
    //    isMonitoring = true;
    //    previousSensorVal = HIGH;
    //} else if(sensorVal == LOW && previousSensorVal == HIGH) {
    //    isMonitoring = false;
    //    previousSensorVal = LOW;
        // Turn all LED off if previousSensorVal was HIGH
    //    for( int i = 0; i < nrOfLeds; i++ ) {
    //      digitalWrite( leds[i][0], LOW );
    //      leds[i][1] = LOW;
    //    }
    //} else if(sensorVal == LOW) {
    //    isMonitoring = false;
    //    previousSensorVal = LOW;
    //}

    if(isMonitoring) {
        readToggleString();
    } else {
        // ledIndex, delay
//        standbyBlink(5, 2000);
        // Clear whatever is send so far from the serial buffer
        while (Serial.available() > 0) {
            Serial.read();
        }
    }
}

/*
Toggle the state of the actual LED
and store the new state in the leds array
*/
void toggleLed(int ledIndex) {
    // HIGH is LED on, LOW is LED off
    int ledPin = leds[ledIndex][0];
    int ledState = leds[ledIndex][1];
    if( ledState == HIGH) {
        digitalWrite(ledPin, LOW);
        leds[ledIndex][1] = LOW;
    } else {
        digitalWrite(ledPin, HIGH);
        leds[ledIndex][1] = HIGH;
    }
}

/*
Incrementally turn all the LEDs on.
Turn LED 1 on, wait a second, turn LED 2 on, wait a second ...
when all on, start over but now turning them off.
*/
void incrementLeds(int delayMillis) {
    for( int i = nrOfLeds; i >= 0; i-- ) {
        toggleLed(i);
        delay(delayMillis);
    }
}

/*
Rotate the burning LED in series
Turn LED 1 on, wait a second, turn LED 1 off
Turn LED 2 on, wait a second, turn LED 2 off...
*/
void rotateLeds(int delayMillis) {
    for( int i = 0; i < nrOfLeds; i++ ) {
        toggleLed(i);
        delay(delayMillis);
        toggleLed(i);
    }
}

void standbyBlink(int ledIndex, long interval) {
  unsigned long currentMillis = millis();
  
  if(currentMillis - previousMillis > interval) {
    previousMillis = currentMillis;
    toggleLed(ledIndex);
  }
}

void readToggleString() {
    while (Serial.available() > 0) {
        int newStates[nrOfLeds];
        String debug = "Receiving ";
        char received[nrOfLeds];
        int lf = 10;

        // Read 6 bytes (if there are 6 LEDS) terminated by a linefeed
        Serial.readBytesUntil(lf, received, nrOfLeds);        
        // Print for debugging
        Serial.println( debug + received );
        
        // Received should contain a 0 or a 1 for each LED, corresponding to HIGH or LOW
        for( int i = 0; i < nrOfLeds; i++ ) {
          //int ledIndex = nrOfLeds - i; // 010101010101
          //Serial.println( i);
          //Serial.println( ledIndex );
          if( received[i] == '1') {
              newStates[i] = HIGH;
          } else {
              newStates[i] = LOW;
          }
          digitalWrite( leds[i][0], newStates[i] );
          leds[i][1] = newStates[i];
        }

        // Pauze for stability
        delay(100);
    }
}

