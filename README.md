# README


# Installing

* install /etc/init/rekelboxOpenPort.conf
* install /etc/init/rekelboxSendCpu.conf
* install /etc/init/rekelboxSendCpuPerc.sh

## Running

* run ```sudo service rekelboxSendCpu start```


## TODO

* Web interface with test mode, debug toggle
* testing on web interface on Windows: https://jfrmilner.wordpress.com/2012/12/30/powershell-power-sockets-arduinorf-part-3-use-powershell-to-send-serial-commands-to-your-arduino/
* Cpu services
* Idle led blink


## Burn CPU

```
stress --cpu 1 -v
stress --cpu 2
```

