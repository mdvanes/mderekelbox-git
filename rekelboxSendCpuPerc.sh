#!/bin/bash

# the function "round()" was taken from 
# http://stempell.com/2009/08/rechnen-in-bash/

# the round function:
round()
{
echo $(printf %.$2f $(echo "scale=$2;(((10^$2)*$1)+0.5)/(10^$2)" | bc))
};

# run this first:
# stty -F /dev/ttyUSB0 cs8 9600 ignbrk -brkint -icrnl -imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe -echok -echoctl -echoke noflsh -ixon -crtscts


# get CPU load from top

# source: http://ubuntuforums.org/showthread.php?t=2084752
# starting at line 7: add everything under column 9 (the column %CPU), and print the result divided by 2 (the amount of cores)
cpuload=$(top -bn 1 | awk 'NR>7{s+=$9} END {print s/2}')
echo "cpu load = $cpuload"

# take line 3, take colum 9 (the idle column), print 100 - idle (= total CPU)
# should theoretically be faster than the option above, but for some reason, running top with -bn 1, totally ignores most processes
#cpuload=$(top -bn 1 | awk 'BEGIN{FS="[ \t%]+"} NR==3{ print 100-$9 }')
#echo "cpu load = $cpuload"
#echo $(top -bn 1 | awk 'BEGIN{FS="[ \t%]+"} NR==3{ print $9 }')
#echo $(top -bn 1)

#  100  50  
#    6   3 = 50/100 * 6
# loaded scaled to amount of leds (100 / 6 = 16.66..) or (100 / 10 leds = 10)
scaledload=$(echo $cpuload/100*10 | bc -l)
echo "scaled load = $scaledload"

nrOfLeds=$(round $scaledload 0);
echo "nr of leds = $nrOfLeds"

request=""
for (( currentLed=1; currentLed<=10; currentLed++ ))
do
	bit=0
	# http://www.tldp.org/LDP/abs/html/refcards.html
	if [ $currentLed -le $nrOfLeds ]
	then
		bit="1"
	fi
	request=$request$bit
done

echo "request: '$request'"

echo $request > /dev/ttyUSB0

